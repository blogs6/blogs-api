package controllers

import (
	beego "github.com/beego/beego/v2/server/web"
)

type BaseController struct {
	beego.Controller
}

func (c *BaseController) json(code int32, msg string, data ...interface{}) {
	//type returnDataStruct struct {
	//	Code int32	`json:"code"`
	//	Msg  string	`json:"msg"`      // 搞了一天是这里没大写
	//	Data interface{}	`json:"data"` //`json:map`
	//}
	//
	//returnJson := returnDataStruct{
	//	Code: code,
	//	Msg:  msg,
	//	Data: data,
	//}
	//
	//
	//ret, err := json.Marshal(returnJson)
	//if err != nil {
	//	fmt.Println("error:", err)
	//}
	//c.Ctx.ResponseWriter.Write(ret)
	//c.StopRun()

	var serve = make(map[string]interface{})

	serve["code"] = code
	serve["msg"] = msg
	if len(data) > 0 && data[0] != nil {
		serve["data"] = data[0]
	} else {
		serve["data"] = make(map[string]interface{})
	}
	//returnJSON,_ := json.Marshal(serve)

	c.Data["json"] = serve
	c.ServeJSON()
	c.StopRun()
}
