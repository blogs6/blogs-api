package controllers

import (
	"blogs-api/models"
	"blogs-api/utils"
	"encoding/json"
	"fmt"
	"github.com/beego/beego/v2/core/logs"
	"regexp"
)

// LoginController 登录控制器
type LoginController struct {
	BaseController
}

// Login 登录
func (c *LoginController) Login() {
	//username := c.GetString("username", "")

	// 声明map
	//var datas = make(map[string]interface{})

	user := &models.User{}
	err := json.Unmarshal(c.Ctx.Input.RequestBody, &user)

	if err != nil {
		c.json(4000, "参数错误")
	}

	if user.Email == "" || user.Password == "" {
		c.json(4000, "请输入邮箱和密码")
	}

	pattern := `\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*` //匹配电子邮箱
	reg := regexp.MustCompile(pattern)

	emailReg := reg.MatchString(user.Email)
	if emailReg == false {
		c.json(4000, "请输入正确的邮箱")
	}

	info, res := models.QueryUserInfo(user.Email)
	if res == false {
		c.json(6001, "无此用户,请去注册")
	}

	if info.Password != utils.Md5(user.Password) {
		c.json(6003, "密码错误")
	}

	//var serve = make(map[string]interface{})
	//
	//serve["code"] = 200
	//serve["msg"] = "成功"
	//serve["data"] = datas
	//c.Data["json"] = serve
	//c.ServeJSON()
	//c.StopRun()

	//numres := models.SetCache("num",12,100000)

	//serErr := utils.InitRedis().Set("list-nums",12,0).Err()

	//logs.Debug(serErr)

	c.json(200, "操作成功", info)
}

// 注册
func (c *LoginController) Register() {
	user := &models.User{}
	err := json.Unmarshal(c.Ctx.Input.RequestBody, &user)

	fmt.Sprintln(user)
	logs.Debug(user)

	if err != nil {
		c.json(4000, "参数错误")
	}

	if user.Email == "" || user.Password == "" {
		c.json(4000, "请输入邮箱和密码")
	}

	pattern := `\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*` //匹配电子邮箱
	reg := regexp.MustCompile(pattern)

	emailReg := reg.MatchString(user.Email)
	if emailReg == false {
		c.json(4000, "请输入正确的邮箱")
	}

	info, res := models.QueryUserInfo(user.Email)
	if res == true {
		c.json(6002, "用户已存在")
	}

	fmt.Println(info)

	id, insertErr := models.InsertUser(user.Name, user.Email, user.Password)
	if insertErr == true {
		c.json(200, "注册成功", id)
	} else {
		c.json(201, "注册失败", id)
	}

}



