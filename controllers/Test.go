package controllers

import "blogs-api/models"

type TestController struct {
	BaseController
}

/**
 * 测试函数
 */
func (c *LoginController) Add() {
	//user := models.Article{}

	//id := models.CreateArticle("test", "title", "content")// 新增
	//articles := models.QueryArticleAll() // 查询所有
	var serve = make(map[string]interface{})

	var article, err = models.QueryArticleById(100)

	if err == false {
		serve["data"] = ""
		serve["code"] = "0000"
		serve["msg"] = "操作失败"

		c.Data["json"] = serve
		c.ServeJSON()
	}

	serve["data"] = article
	serve["code"] = "0000"
	serve["msg"] = "操作成功"

	c.Data["json"] = serve
	c.ServeJSON()

	//c.Ctx.WriteString("call model success!")
}
// 获取一条数据
func (c *LoginController) QueryById() {
	id, _ := c.GetInt("id")

	article, err := models.QueryArticleById(id)
	var serve = make(map[string]interface{})

	if err == false {
		serve["code"] = "0001"
		serve["msg"] = "操作失败"
		serve["data"] = ""
	}

	//tt := article.Create_time

	//timeLayout := "2006-01-02 15:04:05"
	//timeStr := time.Unix(tt, 0).Format(timeLayout) // 时间戳转时间格式
	//article.Create_time = timeStr

	serve["code"] = "0001"
	serve["msg"] = "操作成功"
	serve["data"] = article

	c.Data["json"] = serve
	c.ServeJSON()

}

