package models

import (
	"blogs-api/utils"
	"github.com/beego/beego/v2/client/orm"
	"time"
)

type User struct {
	//Id  uint64 `gorm:"column:id;autoIncrement;comment:'主键'"` //写成AUTO_INCREMENT也可以
	//Id            int    `json:"id"`
	Id            int    `json:"id" orm:"description(主键id)"`
	Name          string `json:"name"`
	Email         string `json:"email"`
	Password      string `json:"password"`
	Status        int64  `json:"status"`
	CreateTime    int64  `json:"create_time"`
	UpdateTime    int64  `json:"update_time"`
	LastLoginTime int64  `json:"last_login_time"`
}

// 查找用户信息
func QueryUserInfo(email string) (User, bool) {
	o := orm.NewOrm()
	user := User{Email: email}
	res := o.Read(&user, "Email")

	if res == orm.ErrNoRows {

		// 未找到数据
		return user, false
	} else if res == orm.ErrMissPK {
		// 未找到主键
		return user, false
	} else {
		return user, true
	}
}

// 创建用户
func InsertUser(name string, email string, password string) (int64, bool) {
	o := orm.NewOrm()

	user := new(User)
	user.Name = name
	user.Email = email
	user.Password = utils.Md5(password)
	user.Status = 1
	user.CreateTime = time.Now().Unix()

	id, err := o.Insert(user)
	if err == nil {
		return id, true
	} else {
		return id, false
	}

}
