package models

import (
	"fmt"
	"github.com/beego/beego/v2/client/orm"
	"time"
)

type Article struct {
	Id         int    `json:"id" orm:"auto;pk;description(文章主键id)"`
	Author     string `json:"author" orm:"size(16);description(作者)" `
	Title      string `json:"title" orm:"size(64)"`
	Content    string `json:"content" orm:"text"`
	CreateTime int64  `json:"create_time" orm:"size(20)"`
}

// 查询所有
func QueryArticleAll() []Article {
	o := orm.NewOrm()
	qs := o.QueryTable("article")

	var articles []Article
	count, err := qs.Filter("id__gt", 0).
		// OrderBy() 参数 - 代表倒序
		OrderBy("-id").
		All(&articles)
	if err == nil {
		fmt.Printf("count", count)
	}
	return articles
}

// 查询(id)
func QueryArticleById(id int) (Article, bool) {
	o := orm.NewOrm()
	article := Article{Id: id}
	err := o.Read(&article)
	if err == orm.ErrNoRows {
		fmt.Println("查询不到~")
		return article, false
	} else if err == orm.ErrMissPK {
		fmt.Println("找不到主键~")
		return article, false
	} else {
		return article, true
	}
}

// 创建
func CreateArticle(author string, title string, content string) int64 {
	o := orm.NewOrm()

	article := new(Article)
	article.Author = author
	article.Title = title
	article.Content = content
	article.CreateTime = time.Now().Unix()

	id, err := o.Insert(article)
	if err == nil {
		fmt.Println("创建成功~")
		return id
	} else {
		fmt.Println("创建失败~")
		return id
	}
}

// 更新
func UpdateArticleById(id int, table string, fields map[string]interface{}) bool {
	o := orm.NewOrm()
	_, err := o.QueryTable(
		table).Filter(
		"Id", id).Update(
		fields)
	if err == nil {
		fmt.Println("更新成功~")
		return true
	}
	fmt.Println("更新失败~")
	return false
}

// 删除(id)
func DeleteArticle(id int) bool {
	o := orm.NewOrm()
	article := Article{Id: id}
	num, err := o.Delete(&article)
	if err == nil {
		fmt.Println("删除影响的行:", num)
		return true
	} else {
		return false
	}
}
