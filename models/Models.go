package models

import (
	"github.com/beego/beego/v2/client/orm"
	beego "github.com/beego/beego/v2/server/web"
)

func Init() {
	// 用户名
	dbuser, _ := beego.AppConfig.String("dbuser")
	// 密码
	dbpassword, _ := beego.AppConfig.String("dbpassword")
	// host
	dbhost, _ := beego.AppConfig.String("dbhost")
	// 端口
	dbPort, _ := beego.AppConfig.String("dbPort")
	// 数据库名称
	dbname, _ := beego.AppConfig.String("dbname")

	//dbcharset,_ := beego.AppConfig.String("dbcharset")
	//dbmaxidel, _ := beego.AppConfig.Int("dbmaxidel") // Int会返回两个值int,err,不使用err就用_代替
	//dbmaxcon, _ := beego.AppConfig.Int("dbmaxcon")

	orm.Debug = true

	//models.Init()

	orm.RegisterDriver("mysql", orm.DRMySQL)
	db := dbuser + ":" + dbpassword + "@tcp(" + dbhost + ":" + dbPort + ")/" + dbname + "?charset=utf8"
	//"root:root@tcp(39.102.129.49:3306)/blogs?charset=utf8"
	orm.RegisterDataBase("default", "mysql", db) //密码为空格式
	//orm.RegisterDataBase("default", "mysql", "用户:密码@tcp(IP:端口)/数据库名称?charset=utf8") //密码为空格式

	prefix, _ := beego.AppConfig.String("dbprefix")
	// 注册model表
	//orm.RegisterModel()
	// 注册model带表前缀
	orm.RegisterModelWithPrefix(prefix, new(User), new(Article))
	// 自动创建表 参数二为是否开启创建表 是否强制更新   参数三是否更新表
	orm.RunSyncdb("default", false, true)
}
