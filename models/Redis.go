package models

import (
	"blogs-api/utils"
	"bytes"
	"context"
	"encoding/gob"
	"errors"
	"github.com/beego/beego/v2/client/cache"
	_ "github.com/beego/beego/v2/client/cache/redis"
	"github.com/beego/beego/v2/core/logs"
	"time"
)

var cc cache.Cache

func InitRedis() {
	var err error

	// 异常
	defer func() {
		if p := recover(); p != nil {
			cc = nil
		}
	}()

	//host, _ := beego.AppConfig.String("redisHost")
	//port, _ := beego.AppConfig.String("redisPort")
	//cc, err = cache.NewCache("redis",`{"conn":"`+host+`":`+port+`}`)
	cc, err = cache.NewCache("redis",  `{"key":"blogs","conn":"127.0.0.1:6379","dbNum":"0","password":""}`)
	if err != nil {
		//logs.Error("Connect to the redis host " + host + " failed")
		logs.Error(err)
	}
	//return cc
}

// SetCache
func SetCache(key string, value interface{}, timeout int) error {
	data, err := Encode(value)
	if err != nil {
		return err
	}
	if cc == nil {
		return errors.New("cc is nil")
	}

	defer func() {
		if r := recover(); r != nil {
			utils.LogError(r)
			cc = nil
		}
	}()

	timeouts := time.Duration(timeout) * time.Second
	err = cc.Put(context.TODO(),key, data, timeouts)
	if err != nil {
		utils.LogError(err)
		utils.LogError("SetCache失败，key:" + key)
		return err
	} else {
		return nil
	}
}

func GetCache(key string, to interface{}) error {
	if cc == nil {
		return errors.New("cc is nil")
	}

	defer func() {
		if r := recover(); r != nil {
			utils.LogError(r)
			cc = nil
		}
	}()

	data, _ := cc.Get(context.TODO(),key)
	if data == nil {
		return errors.New("Cache不存在")
	}

	err := Decode(data.([]byte), to)
	if err != nil {
		utils.LogError(err)
		utils.LogError("GetCache失败，key:" + key)
	}

	return err
}

// DelCache
func DelCache(key string) error {
	if cc == nil {
		return errors.New("cc is nil")
	}
	defer func() {
		if r := recover(); r != nil {
			//fmt.Println("get cache error caught: %v\n", r)
			cc = nil
		}
	}()
	err := cc.Delete(context.TODO(),key)
	if err != nil {
		return errors.New("Cache删除失败")
	} else {
		return nil
	}
}

// Encode
// 用gob进行数据编码
//
func Encode(data interface{}) ([]byte, error) {
	buf := bytes.NewBuffer(nil)
	enc := gob.NewEncoder(buf)
	err := enc.Encode(data)
	if err != nil {
		return nil, err
	}
	return buf.Bytes(), nil
}

// Decode
// 用gob进行数据解码
//
func Decode(data []byte, to interface{}) error {
	buf := bytes.NewBuffer(data)
	dec := gob.NewDecoder(buf)
	return dec.Decode(to)
}

