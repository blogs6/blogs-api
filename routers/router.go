package routers

import (
	"blogs-api/controllers"
	beego "github.com/beego/beego/v2/server/web"
)

func init() {
    //登录
    beego.Router("/login/login", &controllers.LoginController{},"post:Login")
    // 注册
    beego.Router("/login/register", &controllers.LoginController{},"post:Register")
    beego.Router("/add", &controllers.LoginController{},"post:Add")
    beego.Router("/QueryById", &controllers.LoginController{},"post:QueryById")

}
