package utils

import "github.com/go-redis/redis"


func InitRedis() *redis.Client {
	 client := redis.NewClient(&redis.Options{
		Addr: "127.0.0.1:6379",
		Password: "",
		DB: 0,
	})

	if client == nil {
		return nil
		//return client
	}

	return client
}


