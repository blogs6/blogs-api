module blogs-api

go 1.15

require github.com/beego/beego/v2 v2.0.1

require (
	github.com/go-redis/redis v6.14.2+incompatible
	github.com/go-sql-driver/mysql v1.5.0
	github.com/golang/protobuf v1.4.3 // indirect
	github.com/smartystreets/goconvey v1.6.4
	golang.org/x/sys v0.0.0-20210317091845-390168757d9c // indirect
	google.golang.org/protobuf v1.25.0 // indirect
	gopkg.in/yaml.v2 v2.4.0 // indirect
)
