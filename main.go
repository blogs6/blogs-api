package main

import (
	"blogs-api/models"
	_ "blogs-api/routers"
	"blogs-api/utils"
	beego "github.com/beego/beego/v2/server/web"
	_ "github.com/go-sql-driver/mysql"
)

func init() {

	// 初始化 MySQL
	models.Init()
	// 初始化 Redis
	//models.InitRedis()
	utils.InitRedis()
}

func main() {
	// 启用session
	beego.BConfig.WebConfig.Session.SessionOn = true

	// 关闭模板输出
	//beego.BConfig.WebConfig.AutoRender = false

	beego.Run()
}
